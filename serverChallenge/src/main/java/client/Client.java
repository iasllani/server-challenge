package client;

import messaging.Message;
import util.http.HttpException;
import util.http.HttpRequest;
import util.http.HttpResponse;
import util.string.StringHandler;

import java.io.*;
import java.net.Socket;
import java.util.Date;
import java.util.Scanner;

public class Client implements Serializable {

    private String username;
    private String token;
    private String roomName;

    public Client(String username, String token) throws ClientException {
        if (StringHandler.isNullOrEmpty(username))
            throw new ClientException("Username can't be empty");
        if (StringHandler.isNullOrEmpty(token))
            throw new ClientException("Token can't be empty");
        this.username = username;
        this.token = token;
    }

    public Client() {
    }


    public void connectServer() {
        talkToHttp();
    }

    private void talkToHttp() {
        try {
            authenticateClient();
            getRequest();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (HttpException e) {
            System.out.println(e.getMessage());
        }
    }


    private void authenticateClient() throws IOException, HttpException, ClassNotFoundException {
        Scanner sc = new Scanner(System.in);
        while (token == null) {
            System.out.println("Please enter your username so you will be validated:");
            username = sc.nextLine().trim();
            Socket connection = new Socket("localhost", 8080);
            ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
            out.writeObject(new HttpRequest("GET", "/users/authenticate/" + username, "text/plain", "Close"));
            out.flush();
            HttpResponse response = (HttpResponse) in.readObject();
            if (response.getStatus() == 200) {
                String[] args = response.getMessage().split("--");
                username = args[1];
                token = args[2].trim();
                System.out.println(args[0] + args[1]);
            } else System.out.println(response.getMessage());
        }
        removeAuthenticationOnExit();
    }

    private void getRequest() throws HttpException, ClassNotFoundException, IOException {

        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Please enter the desired URL:");
            String url = sc.nextLine();
            Socket httpConnection = new Socket("localhost", 8080);
            ObjectOutputStream out = new ObjectOutputStream(httpConnection.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(httpConnection.getInputStream());
            out.writeObject(new HttpRequest("POST", url, "text/plain", "Close", this.token));
            out.flush();
            HttpResponse response = (HttpResponse) in.readObject();
            httpConnection.close();
            if (url.contains("join") && response.getStatus() == 200) {
                this.roomName = url.substring(url.lastIndexOf("/") + 1);
                String[] args = response.getMessage().split("--");
                System.out.println(response.getStatus() + " - " + response.getStatusDesc() + ": " + args[0]);
                chat(Integer.parseInt(args[1]));
                notifyLeave();

            } else {
                System.out.println(response.getStatus() + " - " + response.getStatusDesc() + ": " + response.getMessage());
            }

        }

    }


    /*private void getRoom() throws IOException {
        Socket s = new Socket("localhost", 3000);
        new DataOutputStream(s.getOutputStream()).writeUTF(token);
        try {
            room = (ChatRoom) new ObjectInputStream(s.getInputStream()).readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }*/

    private void notifyLeave() throws IOException {
        Socket s = new Socket("localhost", 8080);
        try {
            ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(s.getInputStream());
            out.writeObject(new HttpRequest("POST", "/rooms/leave/" + this.roomName, "text/plain", "Close", this.token));
            out.flush();
            HttpResponse response = (HttpResponse) in.readObject();
            System.out.println(response.getMessage());
            this.roomName = null;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (HttpException ex) {
            System.out.println(ex.getMessage());
        } finally {
            s.close();
        }
    }

    public static void main(String[] args) {
        Client client = new Client();
        client.connectServer();

    }

    private void chat(int port) throws IOException {
        System.out.println("You have entered " + roomName);
        Socket socket = new Socket("localhost", port);
        new Thread(() -> {
            Scanner sc = new Scanner(System.in);
            try {
                new DataOutputStream(socket.getOutputStream()).writeUTF(token);
                ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                out.writeObject(new Message(" has joined the chatroom", this, new Date()));
                while (true) {
                    String content = sc.nextLine();
                    if (content.equals("-leave")) {
                        out.writeObject(new Message(" has left the chatroom", this, new Date()));
                        out.flush();
                        break;
                    }
                    Message msg = new Message(": " + content, this, new Date());
                    out.writeObject(msg);
                    out.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (roomName == null)
                        socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        try {
            while (!socket.isClosed()) {
                ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
                Message m = (Message) in.readObject();
                System.out.println(m);
            }
        } catch (IOException e) {
            //user left room, socket closed by server (ignore)
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

    }

    private void removeAuthenticationOnExit() {
        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> {
                    try {
                        Socket httpConnection = new Socket("localhost", 8080);
                        new ObjectOutputStream(httpConnection.getOutputStream()).writeObject(new HttpRequest("POST", "/users/remove", "text/plain", "Close", this.token));
                        httpConnection.getOutputStream().flush();
                        HttpResponse response = (HttpResponse) new ObjectInputStream(httpConnection.getInputStream()).readObject();
                        System.out.println(response.getStatus() + " - " + response.getStatusDesc() + ": " + response.getMessage());
                        httpConnection.close();
                    } catch (IOException | HttpException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                })
        );
    }

    public String getToken() {
        return this.token;
    }

    public String getUsername() {
        return this.username;
    }

    public String toString() {
        return this.username;
    }

}
