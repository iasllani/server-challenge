package servers;


import connections.HttpConnection;

import java.io.IOException;
import java.net.Socket;
import java.util.Date;
import java.util.logging.Logger;

public class HttpChatServer extends ServerSuper {

    private final Logger LOGGER = Logger.getLogger(this.getClass().getSimpleName());
    private static final int HTTP_PORT = 8080;

    public HttpChatServer() throws IOException, ServerException {
        super(HTTP_PORT);
    }

    public void listenToPort() {
        LOGGER.info("Http Server listening on Port: " + port + ", Date: " + new Date());
        try {
            while (true) {
                Socket connection = serverSocket.accept();
                HttpConnection httpConnection = new HttpConnection(connection);
                tPool.execute(httpConnection);
                //BufferedReader inputStream = new BufferedReader(new InputStreamReader(client.getInputStream()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        try {
            HttpChatServer httpS = new HttpChatServer();
            httpS.listenToPort();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ServerException e) {
            System.out.println(e.getMessage());
        }

    }


}
