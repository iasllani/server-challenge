package servers;

import client.Client;
import connections.MiniHttpConnection;
import connections.TcpConnection;
import messaging.ChatRoom;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class TcpChatServer extends ServerSuper {
    private MiniHttpServer miniHttpServer;
    private List<ChatRoom> rooms;
    private List<Client> authenticated;
    private final Logger LOGGER = Logger.getLogger(this.getClass().getSimpleName());
    private static final int TCP_PORT = 3000;
    private static final int MINI_HTTP_PORT = 8081;

    public TcpChatServer(int port) throws IOException, ServerException {
        super(port);
        rooms = new ArrayList<>();
        authenticated = new ArrayList<>();
        miniHttpServer = new MiniHttpServer(MINI_HTTP_PORT);
    }

    public void listenToPort() {
        LOGGER.info("tcp Listening on Port: " + TCP_PORT + ", Date: " + new Date());

        new Thread(() -> {
            try {
                while (true) {
                    Socket connection = serverSocket.accept();
                    tPool.execute(new TcpConnection(connection, this.getRooms()));
                    System.out.println("New user connected.");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void listenToHttp() {
        LOGGER.info("Mini util.http Server listening on Port: " + MINI_HTTP_PORT + ", Date: " + new Date());
        try {
            while (true) {
                Socket connection = miniHttpServer.getServerSocket().accept();
                miniHttpServer.getTPool().execute(new MiniHttpConnection(connection, this.authenticated, this.rooms, this.getPort()));
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public List<ChatRoom> getRooms() {
        return rooms;
    }

    public List<Client> getAuthenticated() {
        return authenticated;
    }

    public static void main(String[] args) {
        try {
            TcpChatServer tcpS = new TcpChatServer(TCP_PORT);
            tcpS.listenToPort();
            tcpS.listenToHttp();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ServerException ex) {
            System.out.println(ex.getMessage());
        }


    }

}
