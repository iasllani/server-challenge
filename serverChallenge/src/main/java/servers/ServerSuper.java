package servers;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerSuper {
    protected ServerSocket serverSocket;
    protected ExecutorService tPool;
    protected int port;

    public ServerSuper(int port) throws IOException, ServerException {
        if (port < 0)
            throw new ServerException("Port can't be a negative number.");
        this.port = port;
        this.serverSocket = new ServerSocket(port);
        this.tPool = Executors.newFixedThreadPool(50);
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public ExecutorService getTPool() {
        return tPool;
    }

    public int getPort() {
        return port;
    }
}
