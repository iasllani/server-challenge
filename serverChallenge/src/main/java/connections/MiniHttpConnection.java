package connections;

import client.Client;
import messaging.ChatRoom;
import util.http.HttpException;
import util.http.HttpRequest;
import util.http.HttpRequestHandler;
import util.http.HttpResponse;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

public class MiniHttpConnection implements Runnable {

    private Socket socket;
    private List<Client> authenticated;
    private List<ChatRoom> rooms;
    private int tcpPort;

    public MiniHttpConnection(Socket socket, List<Client> authenticated, List<ChatRoom> rooms, int tcpPort) throws IOException {
        this.socket = socket;
        this.authenticated = authenticated;
        this.rooms = rooms;
        this.tcpPort = tcpPort;
    }

    @Override
    public void run() {
        ObjectOutputStream output;
        ObjectInputStream input;
        try {
            input = new ObjectInputStream(socket.getInputStream());
            output = new ObjectOutputStream(socket.getOutputStream());
            System.out.println("MiniHttpRequest");
            HttpRequest request = (HttpRequest) input.readObject();
            HttpRequestHandler handler = new HttpRequestHandler(authenticated, rooms, tcpPort);
            HttpResponse response = handler.handle(request);
            output.writeObject(response);
            output.flush();

        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (HttpException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
