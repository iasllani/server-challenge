package connections;

import messaging.ChatRoom;
import messaging.Message;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

public class TcpConnection implements Runnable {
    private Socket socket;
    private List<ChatRoom> rooms;
    private ChatRoom room;

    public TcpConnection(Socket socket, List<ChatRoom> rooms) {
        this.socket = socket;
        this.rooms = rooms;
    }

    @Override
    public void run() {
        try {
            String token = new DataInputStream(socket.getInputStream()).readUTF();
            getRoom(token);
            chat();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    private void chat() {
        try {
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            while (true) {
                Message m = (Message) in.readObject();
                room.addMessage(m);
                if (!m.getContent().contains(":") && m.getContent().contains("left"))
                    break;
            }
        } catch (IOException | ClassNotFoundException e) {
        } finally {
            try {
                room.getUsers().remove(this);
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ObjectOutputStream getOutput() throws IOException {
        return new ObjectOutputStream(socket.getOutputStream());
    }

    private void getRoom(String token) throws IOException {
        room = rooms.stream().filter(m -> m.getClients().stream().anyMatch(client -> client.getToken().equals(token))).findFirst().orElse(null);
        if (room != null)
            room.addConnection(this);
    }
}
