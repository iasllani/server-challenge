package connections;

import util.http.HttpException;
import util.http.HttpRequest;
import util.http.HttpRequestHandler;
import util.http.HttpResponse;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;


public class HttpConnection implements Runnable {
    private Socket client;

    public HttpConnection(Socket client) {
        this.client = client;
    }

    @Override
    public void run() {
        ObjectOutputStream output;
        ObjectInputStream input;
        try {
            output = new ObjectOutputStream(client.getOutputStream());
            input = new ObjectInputStream(client.getInputStream());
            System.out.println("HttpConnection");
            HttpRequest request = (HttpRequest) input.readObject();
            HttpRequestHandler handler = new HttpRequestHandler();
            HttpResponse response = handler.validateRequest(request);

            if (response.getStatus() == 200) {
                Socket tellMiniHttp = new Socket("localhost", 8081);
                new ObjectOutputStream(tellMiniHttp.getOutputStream()).writeObject(request);
                tellMiniHttp.getOutputStream().flush();
                System.out.println("Good URL");
                response = (HttpResponse) new ObjectInputStream(tellMiniHttp.getInputStream()).readObject();
                tellMiniHttp.close();
            }
            output.writeObject(response);
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (HttpException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
