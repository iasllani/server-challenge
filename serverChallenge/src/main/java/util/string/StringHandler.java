package util.string;

public class StringHandler {
    public static boolean isNullOrEmpty(String string) {
        return string == null || string.trim().isEmpty();
    }
}
