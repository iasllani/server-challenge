package util.http;


import java.io.Serializable;

public class HttpResponse extends HttpSuper implements Serializable {
    private int status;

    public HttpResponse(int status, String contentType, String connectionType, String message) throws HttpException {
        super(contentType, connectionType, message);
        if (status < 0)
            throw new HttpException("Status can't be a negative number.");
        this.status = status;
    }

    @Override
    public String toString() {
        return this.getHttpVersion() + this.status + BUFFER_RESET + super.toString();
    }

    public int getStatus() {
        return this.status;
    }

    public String getStatusDesc() {
        switch (status) {
            case 400:
                return "Bad Request";
            case 409:
                return "Conflict";
            case 404:
                return "Not Found";
            case 401:
                return "Unauthorized";
            case 403:
                return "Forbidden";
            case 200:
                return "OK";
        }
        return "Bad Request";
    }
}
