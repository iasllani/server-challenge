package util.http;

import client.Client;
import client.ClientException;
import messaging.ChatRoom;
import messaging.ChatRoomException;
import messaging.Message;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class HttpRequestHandler {
    private List<Client> authenticated;
    private List<ChatRoom> rooms;
    private int tcpPort;

    public HttpRequestHandler(List<Client> authenticated, List<ChatRoom> rooms, int tcpPort) {
        this.authenticated = authenticated;
        this.rooms = rooms;
        this.tcpPort = tcpPort;
    }

    public HttpRequestHandler() {

    }

    public HttpResponse handle(HttpRequest request) throws HttpException, IOException {
        String url = request.getUrl().toLowerCase();
        HttpResponse validation = validateRequest(request);
        if (validation.getStatus() != 200)
            return validation;
        if (url.startsWith("/users/authenticate/"))
            return authenticateUser(url.substring(url.lastIndexOf("/") + 1));
        else if (url.startsWith("/rooms/create/"))
            return createRoom(url.substring(url.lastIndexOf("/") + 1), request.getMessage().trim());
        else if (url.startsWith("/rooms/join/"))
            return addUserToRoom(request.getMessage().trim(), url.substring(url.lastIndexOf("/") + 1));
        else if (url.startsWith("/rooms/leave/"))
            return removeUserFromRoom(request.getMessage().trim(), url.substring(url.lastIndexOf("/") + 1));
        else if (url.equals("/rooms"))
            return listRooms();
        else if (url.equals("/users/remove"))
            return removeAuthentication(request.getMessage().trim());
        else return availableRooms(request.getMessage().trim());

    }

    public HttpResponse validateRequest(HttpRequest request) throws HttpException {
        String url = request.getUrl();
        String value = url.substring(url.lastIndexOf("/") + 1).trim();
        if (url.startsWith("/users/authenticate/")) {
            if (request.getMethod().equals("GET"))
                return validateString(value);
            return invalidRequest(401, "User is not authorized.");
        }
        //requests below are post only
        if (request.getMethod().equals("GET"))
            return invalidRequest(404, "Are you missing something?");
        if (url.startsWith("/rooms/create/"))
            return validateString(value);
        else if (url.startsWith("/rooms/join/"))
            return validateString(value);
        else if (url.startsWith("/rooms/leave/"))
            return validateString(value);
        else if (url.equals("/rooms"))
            return validRequest("Good URL");
        else if (url.equals("/users/remove"))
            return validRequest("Good URL");
        else return url.equals("/rooms/available") ? validRequest("Good URL") : invalidRequest(404, "Bad URL");

    }

    private HttpResponse validateString(String string) throws HttpException {
        if (string.matches("[A-Za-z0-9]+"))
            return validRequest("Good URL");
        return invalidRequest(400, "Bad URL");
    }

    private HttpResponse authenticateUser(String username) throws HttpException {
        String token;
        if (this.authenticated.stream().anyMatch(t -> t.getUsername().equals(username)))
            return invalidRequest(400, "This user has already been authenticated.");
        try {
            token = username.hashCode() + "";
            this.authenticated.add(new Client(username, token + ""));
        } catch (ClientException e) {
            return invalidRequest(400, e.getMessage());
        }
        return validRequest("Successfully authenticated --" + username + "--" + token);
    }

    private HttpResponse createRoom(String roomName, String token) throws HttpException, IOException {
        Client client = this.authenticated.stream().filter(c -> c.getToken().equals(token)).findFirst().orElse(null);
        try {
            if (client == null)
                return invalidRequest(401, "This user hasn't been authenticated");
            if (this.rooms.stream().anyMatch(room -> room.getName().equals(roomName)))
                return invalidRequest(400, "This room has already been created.");
            this.rooms.add(new ChatRoom(roomName));
        } catch (ChatRoomException e) {
            return invalidRequest(400, e.getMessage());
        }
        return validRequest(client.getUsername() + " successfully created new room " + roomName);
    }

    private HttpResponse addUserToRoom(String token, String roomName) throws HttpException {
        Client client = this.authenticated.stream().filter(c -> c.getToken().equals(token)).findFirst().orElse(null);
        if (client == null)
            return invalidRequest(401, "This user hasn't been authenticated");
        ChatRoom alreadyIn = this.rooms.stream().filter(room -> room.getClients().stream().anyMatch(c -> c.getToken().equals(token))).findFirst().orElse(null);
        if (alreadyIn != null)
            return invalidRequest(409, "This user already is in another room. Please leave " + alreadyIn.getName() + " in order to join another room.");
        ChatRoom chatR = this.rooms.stream().filter(cr -> cr.getName().equals(roomName)).findFirst().orElse(null);
        if (chatR == null)
            return invalidRequest(409, "This room doesn't exist");
        try {
            chatR.addMember(client);
        } catch (ChatRoomException e) {
            return invalidRequest(400, e.getMessage());
        }
        return validRequest("User " + client.getUsername() + " successfully joined " + roomName + "--" + tcpPort);

    }

    private HttpResponse removeUserFromRoom(String token, String roomName) throws HttpException {
        Client client = this.authenticated.stream().filter(tcpC -> tcpC.getToken().equals(token)).findFirst().orElse(null);
        if (client == null)
            return invalidRequest(401, "This user hasn't been authenticated");
        ChatRoom chatR = this.rooms.stream().filter(cr -> cr.getName().equals(roomName)).findFirst().orElse(null);
        if (chatR == null)
            return invalidRequest(409, "This room doesn't exist");
        try {
            chatR.removeMember(client);
        } catch (ChatRoomException e) {
            return invalidRequest(400, e.getMessage());
        }
        return validRequest("User " + client.getUsername() + " successfully removed from room " + roomName);
    }

    private HttpResponse removeAuthentication(String token) throws HttpException {
        Client client = this.authenticated.stream().filter(tcpC -> tcpC.getToken().equals(token)).findFirst().orElse(null);
        if (client == null)
            return invalidRequest(401, "This user hasn't been authenticated");
        this.authenticated.remove(client);
        ChatRoom room = this.rooms.stream().filter(c -> c.getClients().stream().anyMatch(cli -> cli.getToken().equals(token))).findFirst().orElse(null);
        if (room != null) {
            try {
                room.addMessage(new Message(" has left the room", client, new Date()));
                room.removeMember(client);
            } catch (ChatRoomException | IOException e) {
            }
        }
        return validRequest("User " + client.getUsername() + " exited.");
    }

    private HttpResponse listRooms() throws HttpException {
        if (this.rooms.isEmpty())
            return validRequest("No rooms have been created yet.");
        String names = "";
        for (ChatRoom cr : this.rooms)
            names += cr.getName() + "\n";
        return validRequest(names);
    }

    private HttpResponse availableRooms(String user) throws HttpException {
        String names = "";
        if (this.rooms.isEmpty())
            return validRequest("No available rooms.");
        for (ChatRoom cr : this.rooms) {
            List<String> tokens = cr.getClients().stream().map(Client::getToken).collect(Collectors.toList());
            if (!tokens.contains(user))
                names += cr.getName();
        }
        return names.isEmpty() ? validRequest("No available rooms to join") : validRequest(names);
    }

    private HttpResponse invalidRequest(int status, String message) throws HttpException {
        return new HttpResponse(status, "text/plain", "Close", message);
    }

    private HttpResponse validRequest(String message) throws HttpException {
        return new HttpResponse(200, "text/plain", "Close", message);
    }


}
