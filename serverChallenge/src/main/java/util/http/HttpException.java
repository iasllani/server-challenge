package util.http;

public class HttpException extends Exception {

    public HttpException(String msg) {
        super(msg);
    }
}
