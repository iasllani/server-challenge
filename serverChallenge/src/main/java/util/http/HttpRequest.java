package util.http;

import util.string.StringHandler;

import java.io.Serializable;

public class HttpRequest extends HttpSuper implements Serializable {
    private String url;
    private String method;


    public HttpRequest(String method, String url, String contentType, String connectionType, String message) throws HttpException {
        super(contentType, connectionType, message);
        if (StringHandler.isNullOrEmpty(method))
            throw new HttpException("Method can't be empty.");
        this.method = method;
        this.url = url;
    }

    public HttpRequest(String method, String url, String contentType, String connectionType) throws HttpException {
        super(contentType, connectionType);
        if (StringHandler.isNullOrEmpty(method))
            throw new HttpException("Method can't be empty.");
        this.method = method;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public String getMethod() {
        return method;
    }

    @Override
    public String toString() {
        return this.method + this.url + this.getHttpVersion() + BUFFER_RESET + super.toString();
    }

}
