package util.http;

import util.string.StringHandler;

import java.io.Serializable;

public class HttpSuper implements Serializable {
    private final String httpVersion = " HTTP/1.1 ";
    private final String userAgent = "User-agent: IDE Terminal\r\n";
    protected final String BUFFER_RESET = "\r\n";
    protected final String END_HEADERS = "\r\n";
    private String contentType;
    private String connectionType;
    private String message;

    protected HttpSuper(String contentType, String connectionType) throws HttpException {
        if (StringHandler.isNullOrEmpty(contentType))
            throw new HttpException("Content type can't be empty");
        if (StringHandler.isNullOrEmpty(connectionType))
            throw new HttpException("Connection type can't be empty");
        this.contentType = "Content-Type: " + contentType;
        this.connectionType = "Connection: " + connectionType;
    }

    protected HttpSuper(String contentType, String connectionType, String message) throws HttpException {
        this(contentType, connectionType);
        if (StringHandler.isNullOrEmpty(message))
            throw new HttpException("Message is empty.");
        this.message = message;
    }

    public String getHeaders() {
        return this.getContentType() + BUFFER_RESET +
                this.getUserAgent() + BUFFER_RESET +
                this.getConnectionType() + BUFFER_RESET +
                END_HEADERS;
    }

    public String toString() {
        return this.getHeaders() + this.getMessage();
    }

    public String getHttpVersion() {
        return httpVersion;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public String getContentType() {
        return contentType;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public String getMessage() {
        if (this.message == null)
            return "";
        return this.message;
    }

}
