package messaging;

import client.Client;
import connections.TcpConnection;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChatRoom implements Serializable {
    private String name;
    private List<Client> clients;
    private List<Message> messages;
    private List<TcpConnection> users;

    public ChatRoom(String name) throws ChatRoomException {
        if (name == null || name.trim().isEmpty())
            throw new ChatRoomException("Room name is Invalid.");
        this.name = name;
        this.clients = new ArrayList<>();
        this.messages = new ArrayList<>();
        this.users = new ArrayList<>();
//        this.listenToConnections();
    }

//    public void listenToConnections() {
//        new Thread(() -> {
//            try {
//                ServerSocket sSocket = new ServerSocket(roomPort);
//                ExecutorService connections = Executors.newFixedThreadPool(50);
//                while (true) {
//                    Socket connection = sSocket.accept();
//                    connections.execute(new RoomConnection(connection, this));
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }).start();
//    }

    public void addMember(Client c) throws ChatRoomException {
        if (c == null)
            throw new ChatRoomException("User not valid.");
        if (clients.contains(c))
            throw new ChatRoomException("User already exists");
        clients.add(c);

    }

    public void addMessage(Message message) throws IOException {
        messages.add(message);
        //users.stream().filter(user -> user!=u).forEach(r -> r.getOutput().writeObject(message));
        for (TcpConnection user : users) {
            user.getOutput().writeObject(message);
        }
    }

    public void addConnection(TcpConnection user) throws IOException {
        users.add(user);
        for (Message m : messages)
            user.getOutput().writeObject(m);
    }

    public void removeMember(Client c) throws ChatRoomException {
        if (c == null)
            throw new ChatRoomException("User not valid.");
        if (!clients.contains(c))
            throw new ChatRoomException("User not in this room.");
        clients.remove(c);
    }

    public List<Client> getClients() {
        return clients;
    }

    public String getName() {
        return name;
    }

    public List<TcpConnection> getUsers() {
        return users;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ChatRoom && ((ChatRoom) o).getName().equals(this.name);
    }
}
