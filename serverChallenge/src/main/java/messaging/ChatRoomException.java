package messaging;

public class ChatRoomException extends Exception {

    public ChatRoomException(String msg) {
        super(msg);
    }
}
