package messaging;

import client.Client;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
    private Date date;
    private Client sender;
    private String content;

    public Message(String content, Client sender, Date date) {
        this.date = date;
        this.sender = sender;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return this.sender + this.content;
    }
}
